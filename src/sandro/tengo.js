define(
  {
    array: "sandro/nadaMas/array",
    object: "sandro/nadaMas/object",
    string: "sandro/nadaMas/string",
    pss: "sandro/palabrasSinSentido",
    jp: "javaPackages"
  }, 
  function(m) {
    
    var tengoBuilderError = function(msg) {
      return {
        isTengoBuilderError: true,
        toString: function() { return "TengoBuilderError: " + msg }
      }
    }
    
    var queryFunctions = {
      executeQuery: m.pss(
        {
          description: "Run query. Analogous to JDBC's execute query in Statement interface.\n" +
            "You are responsible for closing the result set and the statement",
          params: [
            ["connection", "JDBC connection where it will be run"],
          ],
          returns: "JDBC result set"
        },
        function(connection) {

          var stmt = connection.prepareStatement(this.template)
          this.values.forEach(function(value, i) {
            value.type.apply(null, [stmt, i+1].concat(value.args))
          })
          return stmt.executeQuery()
        }
      ),
      forEach: m.pss(
        {
          description: "Iterate on each result of the query",
          params: [
            ["connection", "JDBC connection where it will be run"],
            ["f", "Function that processes each record in the result set.\n" +
            		"Receives a result set (rs) as parameter. DO NOT CALL rs.next()"]
          ]
        },
        function(connection, f) {
          var rs = this.executeQuery(connection)
          try {
            while(rs.next()) {
              f(rs)
            }
          } finally {
            rs.statement.close()
            rs.close()
          }
        }
      ),
      map: m.pss(
        {
          description: "Map query results to Javascript array",
          params: [
            ["connection", "JDBC connection where it will be run"],
            ["f", "Function that processes each record in the result set.\n" +
             "Receives a result set (rs) as parameter and returns the mapped result. DO NOT CALL rs.next()"]
          ]
        }, 
        function(connection, f) {
          var rv = []
          
          this.forEach(connection, function(rs) {
            rv.push(f(rs))
          })
          
          return rv
        }
      )
    }
    
    var basicType = function(t) {
      return function(preparedStatement) {
        
        preparedStatement["set" + t].apply(
          preparedStatement,
          m.array.fromArgs(arguments,1)
        )
      }
    }
    
    var buildExpression = function(template, values) {
      return {
        template: template,
        values: values
      }
    }
   

    // Generate types
    var PreparedStatementClass = m.jp.java.lang.Class.forName("java.sql.PreparedStatement")
    var types = {
    }
    
    PreparedStatementClass.declaredMethods.map(function(m) {
      return m.name
    }).filter( function(k) {
      return k.startsWith("set")
    }).map( function(k) {
      return k.slice(3)
    }).forEach(function(k) {
      types[k[0].toLowerCase() + k.slice(1)] = basicType(k)
    })
    
    var ansiBuilder = {
      coerce: m.pss(
        {
          description: "Coerce object to expression.\n" +
      		"Rules:\n" +
      		" * number -> double expression\n" +
      		" * string -> symbol expression\n" +
      		" * boolean -> symbol expresion\n" +
      		" * array -> coerce each element of the array and generate comma separated expression\n" +
      		" * other cases -> keep the object as is"
        },
        function(o) {
          var t = typeof(o)
          
          if ("number" === t) {
            return this.double(o)
          }
          
          if ("boolean" === t) {
            return this.boolean(o)
          }
          
          if (m.string.isString(o) || m.string.isJavaString(o)) {
            return this.symbol("" + o)
          }
          
          if (m.array.isArray(o)) {
            var that = this
            return this._concat( o, ",") // inner coercion made by _concat
          }
          
          return o
        }
      ),
      unescaped: m.pss( 
        {
          description: "Unescaped expression. Will be sent to the JDBC driver as is",
          params: [
            [ "raw", "String with the expression" ]
          ],
          returns: "The expression"
        },
        function(raw) {
          return buildExpression(raw, [])
        }
      ),
      symbol: m.pss(
        {
          description: "Expression denoting a symbol (such as table name or column name)",
          params: [
            [ "name", "Symbol name" ]
          ]
        },
        function(name) {
          if (name.indexOf("\"") != -1) {
            throw tengoBuilderError("Symbol [" + name + "] cannot have double quotes")
          }
          return this.unescaped(" \"" + name + "\" ")
        }
      ),
      tuple: m.pss( 
        { 
          description: "Make tuple",
          
          params: [
            [ "args", "multiple params. Function parameter expressions"]
          ],
          returns: "tuple expression"
        },
        function() {
          var params = this.coerce(m.array.fromArgs(arguments))
          return buildExpression(
            "( " + params.template + " ) ",
            params.values
          )
        }
      ),
      invoke: m.pss(
        {
          description: "Invoke function",
          params: [
            [ "fName", "function name" ],
            [ "args", "multiple params. Function parameter expressions"]
          ],
          returns: "expression invoking a function"
        },
        function(fName) {
          
          if ( ! /^[a-zA-Z\d_]+$/.test(fName) ) {
            throw tengoBuilderError("fName [" + fName + "] is not valid")
          }
          var tuple = this.tuple.apply(this, m.array.fromArgs(arguments, 1))
          
          return buildExpression(
            " " + fName + "( " + tuple.template + " ) ",
            tuple.values
          )
        }
      ),
      prefix: m.pss(
        {
          description: "Prefix unary operator",
          params: [
            [ "operator", "String denoting an operator" ],
            [ "expression", "Inner expression"]
          ],
          returns: "Expression with the applied operator"
        },
        function(operator, expression) {
          expression = this.coerce(expression)
          
          return buildExpression(
            operator + " ( " + expression.template + " ) ",
            expression.values
          )
        }
      ),
      postfix: m.pss(
        {
          description: "Postfix unary operator",
          params: [
            [ "operator", "String denoting an operator" ],
            [ "expression", "Inner expression"]
          ],
          returns: "Expression with the applied operator"
        },
        function(operator, expression) {
          expression = this.coerce(expression)
          return buildExpression(
            " ( " + expression.template + " ) " + operator,
            expression.values
          )
        }
      ),
      _concat: function(parts, separator) {
        separator = separator || ""
        var that = this
        parts = parts.map(function(e) { return that.coerce(e)})
        return buildExpression(
          parts.map( function(e) { return e.template } ).join(separator),
          [].concat.apply( [], parts.map( function(e) { 
            return e.values 
          } ) )
        )
      },
      infix: m.pss(
        {
          description: "Infix n-ary operator",
          params: [
            [ "operator", "String denoting an operator" ],
            [ "args", "multiple params. Operator operands" ]
          ],
          returns: "an expression"
        },
        function(operator) {
          var operands = m.array.fromArgs(arguments, 1)
          var inner = this._concat( operands, operator )
          
          return buildExpression(
            " ( " + inner.template + " ) ",
            inner.values
          )
        }
      ),
      alias: m.pss(
        {
          description: "Aliased expression",
          params: [
            [ "alias", "String indicating the alias name" ]
            [ "expression", "Expression to be aliased" ]
          ]
        }, 
        function(alias, expression) {
          if ( ! /^[a-zA-Z\d_]+$/.test(alias) ) {
            throw tengoBuilderError("alias [" + alias + "] is not valid")
          }
          expression = this.coerce(expression)
          return buildExpression(
            " ("+ expression.template + ") AS " + alias,
            expression.values
          )
        }
      ),
      select: m.pss(
        {
          description: "SELECT expression",
          params: [ ["params", {
            columns: "Columns in the result set. Expression or expression array. Defaults to unescaped(\"*\")",
            from: "FROM clauses. Expression or expression array. Usually symbols representing tables",
            where: "expression with WHERE clause",
            group_by: "expression or expression array with GROUP BY clauses",
            having: "expression with HAVING clause",
            order_by: "expression or expression array with ORDER BY clauses",
            limit: "expresion with LIMIT CLAUSES"
          } ] ],
          returns: "an queryable expression"
        },
        function(params) {
          var that = this
          var parts = []
          var addPart = function(name, expression) {
            if (expression) {
              parts.push(that.unescaped(name))
              parts.push(that.coerce(expression))
            }
          }
          
          addPart("SELECT", params.columns || this.unescaped("*"))
          addPart("FROM", params.from)
          addPart("WHERE", params.where)
          addPart("GROUP BY", params.group_by)
          addPart("HAVING", params.having)
          addPart("ORDER BY", params.order_by)
          addPart("LIMIT", params.limit)
          
          return m.object.joined( this._concat(parts, " "), queryFunctions )
        }
      ),
      insert: m.pss(
        {
          description:"INSERT expression",
          params: [ ["params", {
            table: "Table to be inserted",
            columns: "Columns to be inserted. Expression array",
            values: "Tuple or array of tuples to be inserted. Build using the .tuple expression",
            select: "SELECT statement with values to be inserted"
          } ] ],
          returns: "The INSERT expression"
        },
        function(params) {
          var that = this
          var parts = []
          var addPart = function(pre, expression, post) {
            if (expression) {
              parts.push(that.unescaped(pre || ""))
              parts.push(that.coerce(expression))
              parts.push(that.unescaped(post || ""))
            }
          }
          
          addPart("INSERT INTO", params.table)
          addPart("(", params.columns,")")
          addPart("VALUES", params.values)
          addPart("", params.select)
          
          return m.object.joined( this._concat(parts, " "), {
            do: m.pss(
              {
                description: "do on connection",
                params: [["connection", "Connection where action will be run"]],
                returns: "Generated keys (as returned by JDBC driver)"
              }, 
              function(connection) {
                var stmt = connection.prepareStatement(this.template)
                try {
                  this.values.forEach(function(value, i) {
                    value.type.apply(null, [stmt, i+1].concat(value.args))
                  })
                  
                  stmt.executeUpdate()
                  
                  var keyRS = stmt.generatedKeys
                  var rv = []
                  while (keyRS.next()) {
                    rv.push(keyRS.getInt(1))
                  }
                  return rv
                  
                } finally {
                  stmt.close()
                }
              }
            )
          } )
        }
      ),
      update: m.pss(
        {
          description: "UPDATE expression",
          params: [ ["params", {
            table: "Table to be updated",
            set: "array of pairs [column expression,value expression] to be set",
            where: "where expression"
          } ] ]
        },
        function(params) {
          var that = this
          var parts = []
          var addPart = function(pre, expression, post) {
            if (expression) {
              parts.push(that.unescaped(pre || ""))
              parts.push(that.coerce(expression))
              parts.push(that.unescaped(post || ""))
            }
          }
          
          addPart("UPDATE", params.table)
          
          var setParts = params.set.map(function(v){
            var k = that.coerce(v[0])
            var v = that.coerce(v[1])
            return buildExpression(
              k.template + " = " + v.template,
              k.values.concat(v.values)
            )
          })
          addPart("SET", setParts)
          
          addPart("WHERE", params.where)
          
          return m.object.joined( this._concat(parts, " "), {
            do: m.pss(
              {
                description: "do on connection",
                params: [["connection", "Connection where action will be run"]]
              }, 
              function(connection) {
                var stmt = connection.prepareStatement(this.template)
                try {
                  this.values.forEach(function(value, i) {
                    value.type.apply(null, [stmt, i+1].concat(value.args))
                  })
                  
                  stmt.executeUpdate()
                } finally {
                  stmt.close()
                }
              }
            )
          })
        }
      )
    }
    Object.keys(types).forEach(function(k) {
      ansiBuilder[k] = m.pss(
        { description: "Build " + k + " expression" }, 
        function() {
          return buildExpression(
            " ? ",
            [ {type: types[k], args:m.array.fromArgs(arguments) } ]
          )
        }
      )
    })
    
    var unary_operators = [
      { name: "not", operation: ansiBuilder.prefix },
      { name: "distinct", operation: ansiBuilder.prefix },
      { name: "exists", operation: ansiBuilder.prefix },
      { name: "desc", operation: ansiBuilder.postfix },
      { name: "isnull", operation: ansiBuilder.postfix }
    ]
    unary_operators.forEach(function(d) {
      var name = d.name
      var operator = d.operator || name
      var operation = d.operation
      
      ansiBuilder[name] = m.pss(
        {
          description: "Unary operator " + operator,
          params: [
            ["expression", "inner expresion"]
          ],
          returns: "an expression"
        },
        function(expression) {
          return operation.call(this, operator, expression)
        }
      )
    })
    
    var infix_operators = [
      { name: "sum", operator: "+" },
      { name: "rest", operator: "-" },
      { name: "prod", operator: "*" },
      { name: "div", operator: "/" },
      { name: "module", operator: "%" },
      { name: "is" },
      { name: "like" },
      { name: "glob" },
      { name: "regexp" },
      { name: "match" },
      { name: "in" },
      { name: "eq", operator: "=" },
      { name: "ne", operator: "<>" },
      { name: "gt", operator: ">" },
      { name: "ge", operator: ">=" },
      { name: "lt", operator: "<" },
      { name: "le", operator: "<=" },
    ]
    
    infix_operators.forEach(function(d) {
      var name = d.name
      var operator = d.operator || name
      
      ansiBuilder[name] = m.pss(
        {
          description: "Infix operator " + operator,
          params: [
            [ "args", "multiple params. Operator operands" ]
          ],
          returns: "an expression"       
        },
        function() {
          return this.infix.apply(this, [operator].concat(m.array.fromArgs(arguments)))
        }
      )
    })
    
    var compound_select = [
      {name: "union"},
      {name: "union_all", operator: "UNION ALL"},
      {name: "intersect"},
      {name: "except"}
    ]
    compound_select.forEach(function(d) {
      var name = d.name
      var operator = d.operator || name
      
      ansiBuilder[name] = m.pss(
        {
          description: "Compound select operation " + operator,
          params: [
            [ "args", "multiple params. Operator operands" ]
          ],
          returns: "an queryable expression"       
        },
        function() {
          return m.object.joined( 
            this.infix.apply(this, [operator].concat(m.array.fromArgs(arguments))),
            queryFunctions
          )
        }
      )
    })
    
    return {
      ansiBuilder: m.pss(
        { description: "Expression builder for ANSI SQL (hopefully!)" },
        ansiBuilder
      )
    }
  }
)