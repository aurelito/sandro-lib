define({
  asi : "./asi",
  test : "./test",
  javaPackages : "javaPackages",
  object: "sandro/nadaMas/object"
}, function(m) {
  var fixedWidth = function(s, len, char) {
    if (s.length >= len) {
      return s.substring(0,len)
    }
  
    char = char || " "
    return s + Array( len - s.length + 1 ).join(char) 
  }
  
  return function() {
    var result = m.asi.run(m.test)
    var out = m.javaPackages.java.lang.System.out

    out.println( result.map(
      function(r) { return fixedWidth(r.status, 12) + r.name }
    ).join("\n"))
    
    out.println( result.filter(
      function(r){
        return r.cause
      }
    ).map(function(r) {
      var rv = "\n" + fixedWidth(r.status, 12) + r.name + "\n" + r.cause + "\n"
      if (r.cause.rhinoException) {
        rv += r.cause.rhinoException.scriptStackTrace + "\n"
      }
      return rv
        
    }).join("\n") )
    
    out.println("------------------")
    
    var consolidated = result.reduce(function(ac, r) {
      ac[r.status] = ac[r.status] || 0
      ac[r.status] += 1 
      return ac
    }, {})
    out.println( Object.keys(consolidated).sort().map(function(k) {
      return "" + consolidated[k] + " " + k
    }).join(" - "))

  }
})