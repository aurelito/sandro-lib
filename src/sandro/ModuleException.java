package sandro;

public class ModuleException extends Exception {
	private static final long serialVersionUID = 1L;
	public String moduleId;
	public String cause;
	public boolean isModuleError = true;
	
	public ModuleException(String moduleId) {
		this(moduleId, (String) null);
	}
	
	public ModuleException(String moduleId, String cause) {
		super();
		this.moduleId = moduleId;
		this.cause = cause;
	}

	@Override
	public String toString() {
		return "Error loading " + moduleId + (cause != null ? ". Cause:" + cause : "");
	}
}
