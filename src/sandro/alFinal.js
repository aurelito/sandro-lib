define(
  {
    domino: "domino/index",
    comoTeDire: "sandro/comoTeDire",
    j: "sandro/nadaMas/java",
    module: "module",
    pss: "sandro/palabrasSinSentido",
    string: "sandro/nadaMas/string",
    json: "sandro/nadaMas/json",
    object: "sandro/nadaMas/object",
    jp: "javaPackages"
  },
  function(m) {
    
    var domino = m.domino
    var comoTeDire = m.comoTeDire
    var j = m.j
    var module = m.module
    var pss = m.pss
    
    var JavaString = m.jp.java.lang.String
    var DatatypeConverter = m.jp.javax.xml.bind.DatatypeConverter
  
    var templateHtmlDescHelp = {
      template: "Path to template HTML resource",
      templateHtml: "String with HTML template"
    }
    var templateHtml = pss(
      {
        description: "Page built using domino",
        params: [["desc", templateHtmlDescHelp]],
        returns: "String with template"
      },
      function(desc) {
        if (desc.template) {
          return j.readFully(module.resourceStream(desc.template), "utf-8")
        }
        return desc.templateHtml || ""
      }
    )
    
    var stdPageReturnsHelp = "Function that generates the page using request and response objects"
    var dominoPage = pss(
      { 
        description: "Page built using domino",
        params: [
          ["desc", templateHtmlDescHelp],
          ["f", "Function to generate the page. Receives request, document and window"]
        ],
        returns: stdPageReturnsHelp
        
      }, 
      function(desc, f) {
        var template = templateHtml(desc)
        
        return function(p) {
          var resp = p.response
          p.window = domino.createWindow( template )
          p.document = p.window.document
    
          f(p)
    
          resp.characterEncoding="UTF-8"
          resp.contentType="text/html"
          resp.writer.println(p.document.innerHTML)
        }
      }
    )
    
    var d3Page = pss(
      {
        description:"Page built using d3",
        params: [
          ["desc", templateHtmlDescHelp],
          ["f", "Function to generate the page. Receives object parameter with request, d3, document and window"]
        ],
        returns: stdPageReturnsHelp
      },
      function(desc, f) {
        return dominoPage(desc, function(p) {
          
          p.d3 = comoTeDire({
            name: "external/d3.js",
            scope: {
              "document": p.document, 
              "window": p.window,
            },
            returns: "d3"      
          })
          f(p)
        })
      }
    )
    
    var dynamic = pss(
      {
        description: "Dynamic page built using modules in load path",
        params: [
          ["desc", "Description", {
            base: "Base module to look for pages implementations",
            contextPath: "Servlet context path. Default root context",
            transform: "Optional. Transformation to be applied before calling the function defined in the module. If not set is called with an object with request and response fields"
          }]
        ],
        returns: stdPageReturnsHelp
      },
      function(desc) {
        var base = desc.base
        var contextPath = desc.contextPath || ""
        var transform = desc.transform || function(m) {return m}
        var contextLength = ("" + contextPath).length
        
        return function(p) {
          var path = base + "/" + ("" + p.request.requestURI).substring(contextLength)
          return require([path], function(m) { transform(m)(p) })
        }
      }
    )
    
    var _static = pss(
      {
        description: "Helper for static resources",
        params: [
          ["p", "dictionary with handler parameters"],
        ]
      },
      function(p) {
        var rd = p.request.servletContext.getNamedDispatcher("default")
        rd.forward(p.request, p.response);
      }
    )
    
    var jsCode = pss(
      { 
        description: "Helper to send code to client",
        params: [
          ["desc", {
            "prefix" : "Prefix used to look for the code in the request",
            "published": "Function that evaluates to true if the code can be sent to the client",
          }]
        ],
        returns: "Function taking request and response dictionary that sends the code to the client"
      },
      function(desc) {
        var published = desc.published
        var prefix = desc.prefix 
        
        return function(p) {
          
          if (! ("" + p.request.requestURI).startsWith(prefix) ) {
            resp.status = resp.SC_NOT_FOUND
            return
          }
          
          var resource = "" + p.request.requestURI.substring(prefix.length)
          if (! published(resource)) {
            p.response.status = p.response.SC_NOT_FOUND
            return
          }
          var stream = module.resourceStream(resource)
          
          if (! stream)  {
            p.response.status = p.response.SC_NOT_FOUND
            return
          }
  
          p.response.characterEncoding="UTF-8"
          p.response.contentType="text/javascript"
            
          j.copyEntireStream(stream, p.response.outputStream)
        }
      }
    )
    
    var json = pss(
      {
        description: "Webservice returning json",
        params: [
          ["f", "Function that generates the JSON object. It receives the request and the response as parameters"]
        ]
      },
      function(f) {
        return function(p) {
          p.response.setContentType("application/json")
          p.response.writer.print( m.json.stringify( f(p) ) )
        }
      }
    )
    
    var fullJson = pss(
      {
        description: "POST Webservice receiving JSON in its body and generating JSON as result",
        params: [
          ["f", "Function that generates the JSON object. It receives the parsed JSON request, the original request and the response as parameters"]
        ]
      },
      function(f) {
        return json(function(p) {
          var request = p.request
          var response = p.response
          
          if (request.method != 'POST') {
            response.status = response.SC_NOT_IMPLEMENTED
            return {error: "method " + request.method + " not allowed. Only POST"}
          }
          
          var line = ""
          var result = ""
          while ((line=request.reader.readLine()) != null) {
            result += line
          }
          p.jsonRequest = m.json.parse(result)
          
          return f(p)
        })
      }
    )
    
    var exposeObject = pss (
      {
        description: "Expose object as handlers, one for each method",
        params: [
          ["object", "Object to be exposed"],
          ["prefix", "prefix where the object will be served. Defaults to /"],
          ["transformation", "transformation to apply in order to expose reach method. Defaults to identity"]
        ]
      },
      function(object, prefix, transformation) {
        prefix = prefix || "/"
        return function(p) {
          var request = p.request
          var response = p.response
          
          var path = "" + request.requestURI
          if (! path.startsWith(prefix) )  {
            response.setStatus(response.SC_NOT_FOUND)
            return
          }
          
          var key = path.substring(prefix.length + 1)
          if (! object[key] ) {
            response.setStatus(response.SC_NOT_FOUND)
            return           
          }
          
          transformation = transformation || function(f) { return f }
          
          transformation(object[key])(p)
        }
      }
    )
    
    var basicAuth = pss( 
      {
        description: "Protect resources with basic authentication",
        params: [
          [
            ["handler", "Handler that manages the request on succesfull authorization. It receives request, response, and user as parameters"],
            ["authFun", "Function that takes a user and a password and returns true if the user is authorized"],
            ["realm", "Authorization realm. Defaults to \"Sandro-powered website\""]
          ]
        ]
      }, 
      function(handler, authFun, realm) {

        realm = realm || "Sandro-powered website"
        
        var innerAuthFun = function(authHeader) {
          
          if (authHeader == null) {
            return false  // no auth
          }
          
          if (!authHeader.toUpperCase().startsWith("BASIC ")) { 
            return false // only basic auth 
          }
          
          var userpass = 
            new JavaString( DatatypeConverter.parseBase64Binary( authHeader.substring(6) ) )
          
          userpass = ("" + userpass).split(":")
          
          var user = userpass[0]
          var pass = userpass[1]
          
          return authFun(user, pass) ? user : false
        }
        
        return function(p) {
          var request = p.request
          var response = p.response
          var user = innerAuthFun( request.getHeader("Authorization") )
          if (! user) {
            response.setHeader("WWW-Authenticate", "BASIC realm=\"" + realm + "\"");
            response.sendError(response.SC_UNAUTHORIZED)
            return
          }
          p.user = user
          
          handler(p)
        }
      }
    ) 
    
    var parameters = pss(
      {
        description: "Add extra parameters to be processed",
        params: [
          ["extraParams", "Extra parameters to be added to the dictionary"],
          ["handler", "Nested handler"]
        ]
      }, 
      function(extraParams,handler) {
        return function(p) {
          m.object.copyInto(p, extraParams)
          handler(p)
        }
      }
    )
    
    return pss("Utilities to build views", {
      basicAuth: basicAuth,
      d3Page: d3Page,
      dominoPage: dominoPage,
      dynamic: dynamic,
      exposeObject: exposeObject,
      fullJson: fullJson,
      jsCode: jsCode,
      json: json,
      parameters: parameters,
      "static": _static
    })
  }
)