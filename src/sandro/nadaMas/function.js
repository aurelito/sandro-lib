define([], function() {
  return {
    isFun: function(o) {
      return typeof o === "function"
    }
  }
})