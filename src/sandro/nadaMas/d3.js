define({array:"./array", pss:"sandro/palabrasSinSentido"}, function(m) {
  var pss = m.pss
  
  var identity = pss({description: "Identity function"}, function(d){return d})
  var children = pss({returns: "All child nodes"}, function() { return this.childNodes })
  
  var tagged = pss(
    { 
      description:"Filter to match tag",
      params: [["tag", "tag to look for"]],
      returns: "A function that checks if self has the correct tag"
    }, 
    function(tag) {
      return function() {
        return this.tagName.toUpperCase() === tag.toUpperCase()
      }
    }
  )
  var classed = pss(
    { 
      description:"Filter to match class",
      params: [["klass", "class to look for"]],
      returns: "A function that checks if self has the correct class"
    },
    function(klass) {
      return function() {
        return this.classList.contains(klass)
      }
    }
  )
  return { 
    gupChildren: pss(
      {
        description: "General update pattern implementation (http://bl.ocks.org/mbostock/3808218), that works only on children (not grand-children or other descendants)",
        params: [
          ["sel", "parent nodes selection"],
          ["params", {
            klass: "(optional) Children class",
            tag: "Children tag",
            values: "New values of the selection children",
            key: "Function to calculate the key. Defaults to function(d,i) {return i} (as in D3)",
            onEnter: "(optional) Function to execute with the enter selection. It receives the enter selection and the default enter action as parameters",
            onExit: "(optional) Function to execute with the exit selection. It receives the exit selection and the default exit action as parameters",
            onUpdateOld: "(optional) Function to execute with the selection of the things to be updated before adding new tags. It receives the selection as parameter",
            onUpdate: "(optional) Function to execute with the selection of the things to be updated after adding new tags. It receives the selection as parameter",
          }],
          
        ],
      },
      function(sel, params) {
      
        var klass = params.klass
        var tag = params.tag
        var values = params.values
        var key = params.key || (function(d,i) {return i})
        
        var defaultOnEnter = function(s) {
          var tags = s.append(tag)
          if (klass) {
            tags = tags.classed(klass, true)
          }
          return tags
        }
        var onEnter = params.onEnter || defaultOnEnter
        
        var defaultOnExit = (function(s) {
          return s.remove()
        })
        var onExit = params.onExit || defaultOnExit
        
        var defaultOnUpdateOld = identity
        var onUpdateOld = params.onUpdateOld || defaultOnUpdateOld
        
        var defaultOnUpdate = identity
        var onUpdate = params.onUpdate || defaultOnUpdate
        
        var children = sel.selectAll(function() { return this.childNodes }).filter( tagged(tag) )
        if (klass) {
          children = children.filter(classed(klass))
        }
        children = children.data(values, key)
        
        onUpdateOld( children, defaultOnUpdateOld )
        onEnter( children.enter(), defaultOnEnter )
        onUpdate( children, defaultOnUpdate )
        onExit( children.exit(), defaultOnExit )
      }
    ),
    identity: identity,
    children: children,
    tagged: tagged,
    classed: classed
  }
})