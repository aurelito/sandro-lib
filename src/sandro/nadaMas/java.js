define(["javaPackages"], function(javaPackages) {
  
  var lang = javaPackages.java.lang
  var io = javaPackages.java.io
  
  var copyEntireStream = function(input, output) {
    var buffer = new lang.reflect.Array.newInstance(lang.Byte.TYPE, 1024)
    var length = 0
    
    while ((length = input.read(buffer)) != -1) {
      output.write(buffer,0,length)
    }
  }
  
  return {
    copyEntireStream: copyEntireStream,
    readFully: function(stream, encoding) {
      var baos = new io.ByteArrayOutputStream()
      copyEntireStream(stream, baos)
      return new lang.String(baos.toByteArray(), encoding)
    }
  }
})