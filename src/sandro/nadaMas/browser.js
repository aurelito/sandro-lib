define([], function() {
  return {
    domReady: function(f) {
      // See http://dustindiaz.com/smallest-domready-ever
      var r=function(f){/in/.test(document.readyState)?setTimeout(r,80,f):f()}
      r(f)
    }
  }
})