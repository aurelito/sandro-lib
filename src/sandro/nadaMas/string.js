define( [ "./object", "module" ], function( o, module ) {
  var def = function( name, fun ) {
    o.def( String.prototype, name, fun )
  }
  
  def( "startsWith", function (str){
    if (str.length > this.length) {
      return false
    }
    return this.slice(0, str.length) == str
  })
  
  // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/includes
  def( "includes", function() {
    return String.prototype.indexOf.apply(this, arguments) !== -1;
  })
  
  var $ = {
    isString: function(o) {return typeof o === "string"}
  }
  
  if (module.platform === "sandro-rhino") {
    require(["javaPackages"], function(javaPackages) {
      $.isJavaString = function(o) {
        return o["class"] === javaPackages.java.lang.String
      }
    })
  }
  return $
})