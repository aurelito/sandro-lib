define([], function() {
  var $ = {
    def : function( obj, name, value ) {
      if( !obj[name] ) {
        obj[name] = value
      }
    },
    
    beget: function( proto ) {
      var F = function() {}
      F.prototype = proto
      return new F()
    },
    
    copyInto: function(target, source) {
      Object.keys(source).forEach(function(k) {
        target[k] = source[k]
      })
    },
    
    joined: function() {
      rv = {}

      for (var i = 0; i < arguments.length; i++) {
        $.copyInto(rv, arguments[i])
      }

      return rv
    }
  }
  $.def(Object, "keys", function(object) {
    rv = []
    for (var property in object) {
      if (object.hasOwnProperty(property)) {
        rv.push(property)
      }
    }
    return rv
  })
  return $
})
