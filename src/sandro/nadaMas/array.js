define( [ "./object" ], function( o ) {
  var def = function( name, fun ) {
    o.def( Array.prototype, name, fun )
  }

  def( "flatten", function() {
    var rv = []
    for( var i = 0; i < this.length; i++ ) {
      var e = this[i]
      if( e.constructor === Array ) {
        rv = rv.concat( e.flatten() )
      } else {
        rv.push( e )
      }
    }
    return rv
  } )

  def( "reduce", function( f, initial ) {
    var r = initial
    this.forEach( function( e, i, ary ) {
      r = f( r, e, i, ary )
    } )
    return r
  } )

  def( "forEach", function( f ) {
    for( var i = 0; i < this.length; i++ ) {
      f( this[i], i, this )
    }
  } )

  def( "map", function( f ) {
    var rv = []
    this.forEach( function( elem, i, ary ) {
      rv[i] = f( elem, i, ary )
    } )
    return rv
  } )
  
  def( "findIndex", function( cond0 ) {
    var cond = ( typeof cond0 == "function" ) ? cond0 : function( e ) {
      return e === cond0
    }
    
    for( var i = 0; i < this.length; i++ ) {
      if (cond(this[i], i, this)) {
        return i
      }
    }
    return -1
  })
  
  def( "any", function( cond ) {
    return this.findIndex( cond ) != -1
  })
  
  def( "all", function(cond0) {
    var cond = ( typeof cond0 == "function" ) ? cond0 : function( e ) {
      return e === cond0
    }
    
    return -1 === this.findIndex( function(e, i, ary) { return ! cond(e, i, ary)})
  })

  def( "find", function( cond ) {
    return this[ this.findIndex(cond) ]
  })
  
  var $ = {
    fromArgs: function( args, startIndex ) {

      var rv = [] 
      
      startIndex = startIndex || 0
      for( var i = startIndex; i < args.length; i++ ) {
        rv[i - startIndex] = args[i];
      }
      return rv
    },
    fromIterable: function( iterable ) {
      return $.fromIterator( iterable.iterator() )
    },
    fromIterator: function( iterator ) {
      var rv = []
      while (iterator.hasNext()) {
        rv.push(iterator.next())
      }
      return rv
    },
    isArray: function( o ) {
      return typeof o === "object" && o.constructor === Array
    }
  }
  
  return $
} )