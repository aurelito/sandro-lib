define(
  {
    json: "sandro/nadaMas/json", 
    module: "module", 
    string: "sandro/nadaMas/string", 
    object: "sandro/nadaMas/object",
    fun: "sandro/nadaMas/function"
  }, 
  function(m){

    var registerGuard = null
    
    if (m.module.platform === "sandro-rhino") {
      require(["javaPackages"], function(javaPackages) {
        registerGuard = new javaPackages.java.util.concurrent.locks.ReentrantLock()
      })
    } else {
      registerGuard = {lock:function(){}, unlock:function(){}} // No need for locking in browser
    }
    
    var registerGuarded = function(f) {
      return function() {
        try {
          registerGuard.lock()
          
          return f.apply(this, arguments)
        } finally {
          registerGuard.unlock()
        }
      }
    }
    
    var json = m.json

    var pssFunDesc = {
        description: "Run to show the documentation",
        params: [
          ["options", {"type" : "'short' or 'long'. Default 'long'"}]
        ],
        returns: "String with the documentation"
      }
    
    var pssFun = function(desc) {
      return function(options) {
        
        options = options || {}
        
        if ('short' === options.type) {
          
          var description = desc.description
          var dotIndex = description.indexOf(".")
          if (-1 === dotIndex) {
            dotIndex = description.length
          }
          var enterIndex = description.indexOf("\n")
          if (-1 === enterIndex) {
            enterIndex = description.length
          }
          
          return description.substring(0, Math.min(dotIndex, enterIndex))
        } 
        
        var rv = ""
        if (desc.description && desc.description.length > 0) {
          rv += desc.description + "\n\n"
        }
        if (desc.params && desc.params.length > 0) {
          rv += "Parameters\n"
          rv += desc.params.map(function(p) {
            var paramDesc = undefined;
            var options = undefined;
            if (m.string.isString(p[1])) {
              paramDesc = p[1]
              options = p[2]
            } else {
              paramDesc = "Object parameter"
              options = p[1]
            }
            
            if (options) {
              paramDesc += "\n" + Object.keys(options).sort().map(function(k) {
                return "    " + k + " : " + options[k]
              }).join("\n")
            }
            return "  " + p[0] + " : " + paramDesc 
          }).join("\n")
          rv += "\n\n"
        }
        if (desc.returns && desc.returns.length > 0) {
          rv += "Returns\n  " + desc.returns + "\n\n"
        }
        
        var that = this
        var childrenKeys = Object.keys(that).filter(function(k) { return m.fun.isFun( that[k].pss ) })
        
        if (childrenKeys.length > 0) {
          rv += "Children\n" + childrenKeys.sort().map(function(k) {
            return "  " + k + " : " + that[k].pss( {type:"short"} )
          } ).join("\n")
        }
        
        return rv
      }
    }
    
    var pssMap = {}
    pssMap[json.stringify(pssFunDesc)] = pssFun(pssFunDesc)
    pssMap[json.stringify(pssFunDesc)].pss = pssMap[json.stringify(pssFunDesc)] // Turtles all the way down!
    
    var pssFor = function(desc) {
      if (m.string.isString(desc)) {
        desc = {description: desc}
      }
      var key = json.stringify(desc)
  
      registerGuarded( function(key) {
        if (!pssMap[key]) {
          pssMap[key] = pssFun(desc)
          pssMap[key].pss = pssMap[json.stringify(pssFunDesc)]
        }
      })(key)
      
      return pssMap[key]
    }
    
    var pss = function(desc, obj){
      
      obj.pss = pssFor(desc)
      return obj
    }
    
    return pss(
      {
        description: "It documents a function or object in a standard way and keeps the description only once (even if multiple functions share it)",
        returns: "Same object. Run obj.pss() to get the documentation",
        params: [
          ["desc", "Either string with description or object parameter", {
            description: "Object description",
            returns: "Returning value description",
            params: "List of parameters. Each element contains in the first element a name and then either a description, fields of the parameter, or both (ie: [name, desc, fields])"
          }],
          ["obj", "object to be documented"]
        ]
      }, 
      pss
    )
  }
)