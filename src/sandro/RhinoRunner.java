package sandro;

import java.util.Arrays;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.Function;
import org.mozilla.javascript.Scriptable;

public class RhinoRunner {

	public static void main(String[] args) {

		Context cx = Context.enter();
		cx.setOptimizationLevel(-1);
		Scriptable scope = cx.initStandardObjects();
		StaticHelper.runScript(cx, "/sandro/penumbras.js", scope);
		
		Scriptable penumbras = (Scriptable) scope.get("penumbras", scope);
		Function loadModule = (Function) penumbras.get("loadModule", penumbras);
		Function module = (Function) loadModule.call(cx, scope, scope, args);
		
		args = Arrays.copyOfRange(args, 1, args.length);
		module.call(cx, scope, scope, args);
	}

}
