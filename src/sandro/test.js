define(
  {
    asi: "./asi", 
    penumbras: "./test/penumbras", 
    comoTeDire: "./test/comoTeDire", 
    nadaMas: "./test/nadaMas", 
    palabrasSinSentido: "./test/palabrasSinSentido",
    tengo: "./test/tengo" 
  },
  function(m) {
    return m.asi.suite("", m.penumbras, m.comoTeDire, m.nadaMas, m.palabrasSinSentido, m.tengo)
  }
)