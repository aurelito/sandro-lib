define(["./nadaMas/array"], function(ary) {
  
  var camino = function() {
    var routes = []
    
    var camino = function(p) {
      var request = p.request
      var response = p.response
      routes.find(function(desc) {
        return desc.route(request)
      }).handler(p)
    }
    camino.register = function(route, handler) {
      routes.unshift({route:route, handler:handler})
    }
    return camino
  }
  
  return {
    camino: camino,
    root: camino(),
    path: function(p) {
      return function(request) {
        return p === "" + request.requestURI 
      }
    },
    prefixPath: function(p) {
      return function(request) {
        var path = "" + request.requestURI
        if (path.length < p.length) {
          return false
        }
        return p === path.substring(0,p.length)
      }
    },
    regex: function(r) {
      if (! r instanceof Regexp) {
        r = RegExp(r)
      }
      
      return function(request) {
        return r.exec(request.requestURI)
      }
    },
    method: function(m) {
      return function(request) {
        return m == request.method
      }
    },
    and: function() {
      var conditions = ary.fromArgs(arguments)
      return function(request) {
        return conditions.all(function(c) {
          return c(request)
        })
      }
    },
    sink: function() {
      return function() { return true }
    }
  }
})