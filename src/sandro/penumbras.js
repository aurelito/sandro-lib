var penumbras = (function() {
  
  var isFunction = function(it) {
    return typeof it === "function"
  }
  
  var o = {}
  var isPlainObject = function(it) {
    return it.constructor === o.constructor
  }
  
  var hasProp = function(obj, prop) {
    return Object.prototype.hasOwnProperty.call(obj, prop)
  }
  
  /**
   * Cycles over properties in an object and calls a function for each
   * property value. If the function returns a truthy value, then the
   * iteration is stopped.
   */
  var eachProp = function(obj, func) {
    var prop
    for (prop in obj) {
      if (hasProp(obj, prop)) {
        if (func(obj[prop], prop)) {
          break
        }
      }
    }
  }

  var adaptNamedDeps = function(deps, callback) {
    var posToNames = []
    var depArray = []

    eachProp(deps, function(modName, name) {
      posToNames.push(name)
      depArray.push(modName)
    })

    var newCallback = function() {
      var namedMods = {}
      for (var i = 0; i < arguments.length; i++) {
        namedMods[posToNames[i]] = arguments[i]
      }
      return callback.call(this, namedMods)
    }

    return [ depArray, newCallback ]
  }

  var moduleError = function(moduleId, cause) {
    
    return new Packages.sandro.ModuleException(moduleId, cause)
  }

  var loadedModules = null
  var loadGuard = new java.util.concurrent.locks.ReentrantLock()
  var loadGuarded = function(f) {
    return function() {
      try {
        loadGuard.lock()
        
        return f.apply(this, arguments)
      } finally {
        loadGuard.unlock()
      }
    }
  }
  
  var reset = loadGuarded( function(desc) {
    
    desc = desc || {}
    var newLoadedModules = {}
    
    
    if (desc.doNotReset) {
      desc.doNotReset.forEach( function(m) {
        if (loadedModules[m]) {
          newLoadedModules[m] = loadedModules[m]
        }
      } )
    }
    loadedModules = newLoadedModules
  })
  reset()

  var loadModule = loadGuarded(function(absPath) {
    var resourceStream = function(absPath) {
      return java.lang.Class.forName("sandro.StaticHelper").getResourceAsStream("/"+absPath)
    }
    
    if (!loadedModules[absPath]) {
      var stream = resourceStream(absPath+".js")
      if (! stream) {
        throw moduleError(absPath, "Module not found")
      }
      
      var defineHasBeenRun = false
      
      var absolutize = function(path) {
        if ( "./" === path.substring(0,2) ) {
          return absPath.substring(0, absPath.lastIndexOf("/") + 1 ) +
            path.substring(2)
        }
        if ( "/" === path[0] ) {
          return path.substring(1)
        }
        return path
      }
      
      var require = function(moduleIds, f) {
        
        // If using named deps, adapt accordingly
        if ( isPlainObject(moduleIds) && isFunction(f) ) {
          var depsAndCallback = adaptNamedDeps(moduleIds, f)
          moduleIds = depsAndCallback[0]
          f = depsAndCallback[1]
        }

        
        if ( !moduleIds.map ) {
          throw moduleError(absPath, "moduleIds must be an array") 
        }
        var modules = moduleIds.map(function(modId) {
          modId = absolutize(modId)
          if ("module" === modId ) { return { 
            id:absPath,
            absolutize: absolutize,
            resourceStream: function(path) { return resourceStream(absolutize(path))},
            platform: "sandro-rhino"
          } }
          if ("javaPackages" === modId) {return Packages}
          if ("require" === modId) {return require}
          return loadModule(modId, resourceStream)
        })
        return f.apply(undefined, modules)
      }
      
      var scope = {
        require: require,
        define: loadGuarded( function(moduleIds, f) {
          if (defineHasBeenRun) {
            throw moduleError(absPath, "Define has been run twice for the same module")
          }
          if ( isFunction(moduleIds) ) {
            // AMD module
            var mod = moduleIds
            var exports = {}
            var module = { id:absPath, exports: exports }
            var amdRequire = function(modId) {
              return require([modId], function(m) {return m})
            }
            mod(amdRequire, exports, module)
            loadedModules[absPath] = module.exports
            
          } else {
            loadedModules[absPath] = require(moduleIds, f)
          }
          defineHasBeenRun = true
          return loadedModules[absPath]
        })
      }
      
      org.mozilla.javascript.Context.getCurrentContext().evaluateReader(
        scope, 
        new java.io.InputStreamReader(stream, "UTF-8"),
        absPath + ".js",
        1,
        null
      )
    } 
    if (! loadedModules[absPath]) {
      throw moduleError(absPath, "Module not registered. Has the define function been run?")
    }
    return loadedModules[absPath]
  })
  
  return {
    loadModule: loadModule,
    reset: reset
  }
})()
