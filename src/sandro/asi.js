define(
  {
    ary: "sandro/nadaMas/array", 
    json: "sandro/nadaMas/json",
    string: "sandro/nadaMas/string"
  },
  function(m) {
    var ary = m.ary
    var assertionError = function(message) {
      var rv = new Error("AssertionError: " + message)
      rv.isAssertionError = true
      return rv
    }
    
    var fail = function(message) {
      throw assertionError(message)
    }
    
    var assertions = function(fail) {
      return {
        jsonEqual: function(expected, actual, message) {
          var that = this
          if (!message) {
            message = "Expected " + m.json.stringify(expected) + " but got " + m.json.stringify(actual)
          }
          if (ary.isArray(expected)) {
            this.equal(true, ary.isArray(actual), message)
            this.equal(expected.length, actual.length, message)
            expected.forEach(function(e,i) {
              that.jsonEqual(e, actual[i], message)
            })
          } else if (typeof expected === 'object') {
            this.equal(true, typeof expected === 'object', message)
            
            var expectedKeys = Object.keys(expected)
            expectedKeys.sort()
            
            var actualKeys = Object.keys(actual)
            actualKeys.sort()
            
            this.jsonEqual(expectedKeys, actualKeys, message)
            expectedKeys.forEach(function(k) {
              that.jsonEqual(expected[k], actual[k], message)
            })
          } else {
            this.equal(expected, actual, message)
          }
        },
        equal: function(expected, actual, message) {
          if (! (actual === expected) ) {
            fail(message || ("Expected " + expected + " but got " + actual))
          }
        },
        ok: function(o, message) {
          if (! o) {
            fail(message || ("" + o + " must be a truthfull value"))
          }
        },
        raises: function(action, check) {
          try {
            action()
            fail("Exception expected")
          } catch(e) {
            check(e)
          }
        },
        fail: fail
      }
    }
    
    return {
      run: function(suite) {
        return suite.map(function(t) {
          try {
            t(fail)
            return {
              name: t.testName,
              status: "success"
            }
          } catch (e) {
            return {
              name: t.testName,
              cause: e,
              status: e.isAssertionError ? "failed" : "exception" 
            }
          }
        })
      },
      test: function(name, f) {
        var test = function(fail) {
          f(assertions(fail))
        }
        test.testName = name
        return test
      },
      suite:function(name) {
        var args = ary.fromArgs(arguments,1)
        
        return args.flatten().map(function(t) {
          if (name) {
            t.testName = name + "." + t.testName
          }
          return t
        })
      }
    }
  }
)