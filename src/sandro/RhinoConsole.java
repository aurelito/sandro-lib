package sandro;

import org.mozilla.javascript.tools.shell.Main;

public class RhinoConsole {
	
	public static void main(String[] args) {

		String[] consoleArgs = new String[] {
			"-e", "Packages.sandro.StaticHelper.runScript(Packages.org.mozilla.javascript.Context.enter(), \"/sandro/penumbras.js\", this)",
			"-f", "-"
		};
		Main.main(consoleArgs);
	}

}
