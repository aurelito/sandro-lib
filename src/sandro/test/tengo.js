define(
  {
    asi:"sandro/asi", 
    module: "module",
    tengo: "sandro/tengo",
    jp: "javaPackages"
  },
  function(m) {

    var module = m.module
    var asi = m.asi
    var tengo = m.tengo
    var _ = tengo.ansiBuilder
    
    m.jp.java.lang.Class.forName("org.sqlite.JDBC")
    
    return asi.suite(
      module.id,
      asi.test("doNothing", function($) {
        _.string("aaaa")
      }),
      asi.test("normal and invalid symbols", function($) {
        _.symbol("normal symbol")
        $.raises(
          function() {
            _.symbol("with double quote \"")
          },
          function(e) {
            return !!e.isTengoBuilderError
          }
        )
      }),
      asi.test("unescaped", function($) {
        var expression = _.unescaped("; '")
        $.equal("; '", expression.template)
        $.equal(0, expression.values.length)
      }),
      asi.test("invoke", function($) {
        _.invoke("aFunction", _.string("a string"), _.int(1))
      }),
      asi.test("operands", function($) {
        _.prefix("NOT", false)
        _.postfix("IS NULL", "column")
        _.infix("+", _.int(1), 2)
      }),
      asi.test("alias", function($) {
        _.alias("alias", "symbol")
      }),
      asi.test("basic usage", function($) {
        var connection = m.jp.java.sql.DriverManager.getConnection("jdbc:sqlite::memory:")
        try {
          statement = connection.createStatement()
          statement.executeUpdate("drop table if exists person")
          statement.executeUpdate("create table person (id integer primary key, name string, rank integer)")
          
          values = [_.tuple(_.string("leo"), 1), _.tuple(_.string("yui"), 1), _.tuple(_.string("master"), 2)]
          var keys = values.map(function(v) {
            return _.insert({
              table: "person",
              columns: ["name", "rank"],
              values: v
            }).do(connection)[0]          
          })
          
          var rs = _.select({
            from: "person",
            order_by: _.desc("id")
          }).executeQuery(connection)
          
          $.jsonEqual(
            [{name:"master", id:keys[2]}, {name:"yui", id:keys[1]}, {name:"leo", id:keys[0]}],
            _.select({
              from: "person",
              order_by: _.desc("id")
            }).map(connection, function(r) {
              return {name: "" + r.getString("name"), id:r.getInt("id")}
            })
          )

          $.jsonEqual(
            ["leo"], 
            _.select({
              columns: _.alias("pindonga", "name"),
              from: "person",
              where: _.eq(_.int(keys[0]), "id")
            }).map(connection, function(rs) {
              return "" + rs.getString("pindonga")
            })
          )
          
          $.jsonEqual(
            [{rank:1, c:2}],
            _.select({
              columns: [ _.alias("c",_.invoke("count", _.int(1))), "rank" ],
              from: "person",
              group_by: "rank",
              having: _.gt("c", _.int(1))
            }).map(connection, function(rs) {
              return {c: rs.getInt("c"), rank:rs.getInt("rank")}
            })
          )
          
          _.update({
            table: "person",
            set: [["name", _.string("dan")]],
            where: _.eq("name", _.string("leo"))
          }).do(connection)
          
          $.jsonEqual(
            ["dan"], 
            _.select({
              columns: _.alias("pindonga", "name"),
              from: "person",
              where: _.eq(_.int(keys[0]), "id")
            }).map(connection, function(rs) {
              return "" + rs.getString("pindonga")
            })
          )

        } finally {
          connection.close()
        }
      })
    )
  }
)