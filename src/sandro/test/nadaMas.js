define(
  {
    asi:"sandro/asi", 
    f:"./nadaMas/function", 
    s:"./nadaMas/string",
    o:"./nadaMas/object"
  },
  function(m) {
    return m.asi.suite("", m.f, m.s, m.o)
  }
)