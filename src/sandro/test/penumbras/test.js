define(
  {
    asi: "sandro/asi", 
    array: "sandro/nadaMas/array", 
    javaPackages: "javaPackages", 
    module: "module", 
    require: "require"
  }, 
  function(m) {
    var asi = m.asi
    var array = m.array
    var javaPackages = m.javaPackages
    var module = m.module
    var requireModule = m.require
    return asi.suite(
      module.id, 
      asi.test("require", function($){
        var mods = ["sandro/test/penumbras/absolute", "sandro/test/penumbras/absolute2", "./relative", "./submodule/relative", "./submodule/nested"]
        var check = function(absolute, absolute2, relative, submodule, nested){
          $.equal("absolute", absolute)
          $.equal("absolute2", absolute2)
          $.equal("relative", relative)
          $.equal("submodule", submodule)
          $.equal("nested.submodule", nested)
        }
   
        require(mods, check)
        require(mods, check)
      }),
      asi.test("moduleErrors", function($) {
        (["./noDefine", "./defineTwice"]).forEach(function(m) {
          $.raises(
            function() {
              require([m], function(m){})
            },
            function(e) {
              $.ok(e.isModuleError)
            }
          )
        })
      }),
      asi.test("resourceStream", function($) {
        var io = javaPackages.java.io
        var reader = new io.BufferedReader(new io.InputStreamReader(module.resourceStream("./toto.txt")))
        try { 
          $.equal("TOTOTOTO", "" + reader.readLine())
        } finally {
          reader.close()
        }
      }),
      asi.test("id", function($) {
        // requireJS compatible way of getting the module name. See https://github.com/jrburke/requirejs/issues/352
        $.equal("sandro/test/penumbras/test", module.id) 
      }),
      asi.test("absolutize", function($) {
        $.equal("a", module.absolutize("a"))
        $.equal("sandro/test/penumbras/a", module.absolutize("./a"))
      }),
      asi.test("amd", function($) {
        require(["./amd"], function(amd) {
          $.equal("foo", amd.foo)
          $.equal("submodule", amd.submodule)
        })
      }),
      asi.test("amdReplace", function($) {
        require(["./amdReplace"], function(amdReplaced) {
          $.equal("replaced", amdReplaced)
        })
      }),
      asi.test("platform", function($) {
        $.equal("sandro-rhino", module.platform)
      }),
      asi.test("require module", function($) {
        requireModule(["./relative"], function(r) {
          $.equal("relative", r)
        })
      })
    )
  }
)