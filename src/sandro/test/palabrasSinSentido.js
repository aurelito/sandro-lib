define({
  asi : "sandro/asi",
  module : "module",
  pss : "sandro/palabrasSinSentido",
  string: "sandro/nadaMas/string"
}, function(m) {

  var module = m.module
  var asi = m.asi
  var pss = m.pss
  
  var includes = function($, part, hole) {
    $.ok(hole.includes(part), "[" + part + "] should be included in [" + hole + "]")
  }

  return asi.suite(module.id, 
      
    asi.test("normal usage", function($) {
      var obj = pss({
          description: "Object description",
          returns: "Return description",
          params: [
            ["param1", "desc1"], 
            ["dictParam", {paramOption: "option description"}],
            ["mestizoParam", "mestizo description", {mestizoOption: "mestizo option description"}]
          ]
        }, { 
          childField: pss({description: "Description for first child"}, {})
        } )
      var doc = obj.pss()
      
      var parts = [
        "Object description", 
        "Return description",
        "param1 : desc1",
        "dictParam",
        "paramOption",
        "option description",
        "mestizoParam",
        "mestizo description",
        "mestizoOption",
        "mestizo option description",
        "childField",
        "Description for first child"
      ]
      parts.forEach(function(part) {
        includes($, part, doc)
      })
    }),
    
    asi.test("string description", function($) {
      var obj = pss("string description", {})
      var doc = obj.pss()
      
      includes($, "string description", doc)
    })
  )
})