define(["sandro/asi", "module", "sandro/nadaMas/function"], function(asi, module, f) {
  return asi.suite(
    module.id,
    asi.test("isFun", function($){
      $.ok( f.isFun(function() {}))
      $.ok( ! f.isFun("not a function"))
    })
  )
})