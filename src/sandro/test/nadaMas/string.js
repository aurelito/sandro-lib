define(["sandro/asi", "module", "sandro/nadaMas/string", "javaPackages"], function(asi, module, s, javaPackages) {
  var lang = javaPackages.java.lang
  return asi.suite(
    module.id, 
    asi.test("isString", function($) {
      $.ok( s.isString("a JS string"), "JS string test failed" )
      $.ok( !s.isString(new lang.String("a Java string")), "Java string test failed")
      $.ok( !s.isString({}), "object test failed")
      $.ok( !s.isString(4), "number test failed")
    }),
    asi.test("isJavaString", function($) {
      $.ok( !s.isJavaString("a JS string"), "JS string test failed" )
      $.ok( s.isJavaString(new lang.String("a Java string")), "Java string test failed")
      $.ok( !s.isJavaString({}), "object test failed")
      $.ok( !s.isJavaString(4), "number test failed")
    })

  )
})