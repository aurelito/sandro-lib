define(
  {
    asi: "sandro/asi",
    object: "sandro/nadaMas/object",
    module: "module"
  },
  function(m) {
    return m.asi.suite(
      m.module.id,
      m.asi.test("joined", function($){
        var j = m.object.joined({a:1}, {b:2})
        $.equal(1, j.a)
        $.equal(2, j.b)
      })
    )
  }
)