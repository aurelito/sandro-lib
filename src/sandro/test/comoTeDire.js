define(["sandro/asi", "module","sandro/comoTeDire"], function(asi, module, comoTeDire) {
  return asi.suite(module.id,
    asi.test("stream", function($) {
      var adapter = comoTeDire({
        name: module.absolutize("./comoTeDire/legacy.js"),
        scope: {"window": {}},
        returns: "window.legacy"
      })
      $.equal("legacy",adapter)
    })
  )
})