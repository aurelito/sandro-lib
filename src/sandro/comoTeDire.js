define(["module", "javaPackages", "./nadaMas/java"], function(module, javaPackages, j) {
  var io = javaPackages.java.io
  
  return function comoTeDire(desc) {
    var name = desc.name
    var scope = desc.scope
    var returns = desc.returns
    
    var sw = new io.StringWriter()
    var writer = new io.PrintWriter( sw )
    var stream = module.resourceStream(name)
    try {
      writer.print("function() { ")
      writer.print(j.readFully(stream, "utf-8"))
      writer.println()
      writer.println("; return " + returns + " }")
      
      return org.mozilla.javascript.Context.getCurrentContext().compileFunction(scope, sw.toString(), name, 1, null)()
    } finally {
      stream.close()
    }
  }
})