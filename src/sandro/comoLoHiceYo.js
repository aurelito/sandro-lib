define(["./camino", "./alFinal", "./nadaMas/object", "./nadaMas/string", "./nadaMas/array"], function(c, alFinal, o, s) {
  
  return function(desc) {
    desc=desc||{}
    var app=desc.app || "app"
    var contextPath=desc.contextPath || ""
    var jsPrefix=contextPath + (desc.jsPrefix || "/js/")
    var cssPrefix=contextPath + (desc.cssPrefix || "/css/")
    var imgPrefix=contextPath + (desc.imgPrefix || "/img/")
    var endPoints=desc.endPoints || app + "/endpoint"
    var extraExposedPrefixes=desc.extraExposedPrefixes || []
    var exposedPrefixes = (desc.exposedPrefixes || [
      app + "/shared/",
      "external/",
      "sandro/"
    ]).concat(extraExposedPrefixes)
    var template = desc.template || null
    var templateHtml = desc.templateHtml || null 
    var extraShimConfig = desc.extraShimConfig || {}
    var shimConfig = o.joined(desc.shimConfig || {
      "external/d3": {
        exports: 'd3'
      }
    }, extraShimConfig)
    var mainPage = desc.mainPage || app + "/mainPage"
    if (s.isString(mainPage) || s.isJavaString(mainPage)) {
      mainPage = require([mainPage], function(m) {return m})
    }
    
    var page = function(f) {
      return function(p) {
        
        p.clhyPage = function(f2) {
          (
            alFinal.d3Page(desc, function(p) {
              var req = p.request
              var d3 = p.d3
              var document = p.document
              var window = p.window
              
              p.startClientJs = function(mod) {
                var h = d3.select("head")
                h.append("script").text(
                  "var require = {baseUrl:'"+jsPrefix+"', shim:"+JSON.stringify(shimConfig)+"}"
                )
                h.append("script").attr({
                  "src": jsPrefix + "external/require2.js"
                })
                d3.select("body").insert("script",":first-child").text(
                  "require(['"+mod+"'])"
                )
              }
              
              f2(p)
            })
          )(p)
        }
                
        f(p)
      }
    }
    
    var router = c.camino()
    router.register(
      c.sink(),
      alFinal.dynamic({
        base:endPoints, 
        transform: function(m) {return page(m) },
        contextPath: contextPath
      })
    )
    var mainPageFun = page(mainPage)
    router.register(c.path(contextPath), mainPageFun)
    router.register(c.path(contextPath + "/"), mainPageFun)

    router.register(c.prefixPath(cssPrefix), alFinal.static)
    router.register(c.prefixPath(imgPrefix), alFinal.static)
    router.register(c.path("/favicon.ico"), alFinal.static)
    
    router.register(c.prefixPath(jsPrefix), alFinal.jsCode({
      published: function(resource) {
        resource = "" + resource
        return exposedPrefixes.any(function(prefix) { 
          return resource.startsWith(prefix)
        })
      },
      prefix: jsPrefix
    }))

    return router
  }
  
})