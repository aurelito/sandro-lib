package sandro;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.ContextFactory;
import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.tools.debugger.Main;

/**
 * Servlet that binds rhino to the servlet engine.
 * 
 * It runs /WEB-INF/js/init.js when the servlet is loaded and service.js when
 * each request arrives.
 */
public class RhinoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Scriptable initialScope = null;
	private boolean reload = false;
	private String doNotReload = null;
	private Main rhinoDebugger = null;

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		this.reload = "true".equals(config
				.getInitParameter("reload"));
		
		String doNotReloadDesc = config.getInitParameter("do-not-reload");
		if (doNotReloadDesc == null || "".equals(doNotReloadDesc)) {
			this.doNotReload = "[]";
		} else {
			this.doNotReload = "[\"" + doNotReloadDesc.replaceAll(",", "\",\"") + "\"]";
		}

		this.setupDebugger(config);

		Context cx = Context.enter();
		cx.setOptimizationLevel(-1);
		try {
			Scriptable scope = cx.initStandardObjects();
			StaticHelper.addJavaObjectToScope(scope, "servlet", this);
			StaticHelper.addJavaObjectToScope(scope, "servletConfig", config);

			// StaticHelper.initRequireInScope(cx, scope);
			StaticHelper.runScript(cx, "penumbras.js", scope);
			this.setupApp(cx, scope, "init");
			this.initialScope = scope;
		} finally {
			Context.exit();
		}
	}
	
	@Override
	public void destroy() {
		Context cx = Context.enter();
		cx.setOptimizationLevel(-1);
		try {
			this.setupApp(cx, this.initialScope, "destroy");
			if (this.rhinoDebugger != null) {
				this.rhinoDebugger.dispose();
			}
			super.destroy();
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			Context.exit();
		}
	}
	
	private void setupApp(Context cx, Scriptable scope, String action) {
		
		String jsAction = "app." + action;
		cx.evaluateString(
			scope,
			"var app = penumbras.loadModule('/app')\n" + 
			"if (" + jsAction + ") { " + jsAction + "(servlet, servletConfig) }",
			"rhino_servlet_" + action, 1, null
		);
	}
	
	@Override
	protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		this.myService(req, resp);
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		this.myService(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		this.myService(req, resp);
	}

	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		this.myService(req, resp);
	}

	private void setupDebugger(ServletConfig config) throws ServletException {
		
		try {
			String debugger = config.getInitParameter("debugger");
			
			if ("rhino".equals(debugger)) {

				this.rhinoDebugger = new Main("Sandro server-side debugger");

				this.rhinoDebugger.attachTo(ContextFactory.getGlobal());

				this.rhinoDebugger.pack();
				this.rhinoDebugger.setSize(1200, 900);
				this.rhinoDebugger.setVisible(true);

			} 
		} catch (Exception e) {
			throw new ServletException(e);
		}
		
	}

	protected void myService(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		Context cx = Context.enter();
		cx.setOptimizationLevel(-1);
		try {

			if (this.reload) {
				synchronized(this) {
					if (this.reload) {
						this.setupApp(cx, this.initialScope, "destroy");
						cx.evaluateString(
							this.initialScope, 
							"penumbras.reset({doNotReset:" + this.doNotReload + "})",
							"penumbras_reset", 1, null
						);
						this.setupApp(cx, this.initialScope, "init");
					}
					this._service(req, resp, cx);
				}
			} else {
				this._service(req, resp, cx);
			}
		} catch (RuntimeException e) {
			e.printStackTrace();
			throw e;
		} finally {
			Context.exit();
		}
	}

	private void _service(HttpServletRequest req, HttpServletResponse resp,
			Context cx) {
		Scriptable scope = cx.newObject(this.initialScope);
		scope.setParentScope(this.initialScope);

		StaticHelper.addJavaObjectToScope(scope, "servletRequest", req);
		StaticHelper.addJavaObjectToScope(scope, "servletResponse", resp);
		
		cx.evaluateString(
				scope,
				"penumbras.loadModule('/app').service({request:servletRequest,response:servletResponse})",
				"rhino_servlet_service", 1, null);
	}
}